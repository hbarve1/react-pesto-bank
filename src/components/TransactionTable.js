import React, {useState, } from "react";

// NOTE: I'm assuming this test is not for testing sorting algorithm test, so 
// I'm using basic sorting algo default to javascript.
function TransactionTable({txns}) {
  const [list, setList] = useState([...txns]);
  const [filter, setFilter] = useState("");

  const sort = () => {
    const newList = [...list].sort((a, b) => a.amount-b.amount);
    setList(newList);
  };

  const onChangeFilter = (value) => {
    setFilter(value)
  };

  return (
    <div className="layout-column align-items-center mt-50">
      <section className="layout-row align-items-center justify-content-center">
        <label className="mr-10">Transaction Date</label>
        <input 
          id="date" 
          type="date" 
          value={filter} 
          // defaultValue="2019-11-29" 
          onChange={e => onChangeFilter(e.target.value)} 
          />
        <button className="small">Filter</button>
      </section>

      <div className="card mt-50">
          <table className="table">
              <thead>
              <tr className="table">
                  <th className="table-header">Date</th>
                  <th className="table-header">Description</th>
                  <th className="table-header">Type</th>
                  <th className="table-header">
                      <span id="amount" onClick={sort}>Amount ($)</span>
                  </th>
                  <th className="table-header">Available Balance</th>
              </tr>
              </thead>
              <tbody>
                {Array.from(list).filter(value => {
                  if (filter.length > 0) {
                    return value.date === filter;
                  }
                  return true;
                }).map(({date, description, type, amount, balance}) =>  (
                  <tr key={`${date}-${description}`}>
                    <td>{date}</td>
                    <td>{description}</td>
                    <td>{type === 1 ? "Debit" : "Credit"}</td>
                    <td>{amount}</td>
                    <td>{balance}</td>
                  </tr>
                ))}
              </tbody>
          </table>
      </div>
    </div>
  );
}

export default TransactionTable;
